package com.ufro.actividadtdd.dev.controllers;

import com.ufro.actividadtdd.dev.models.Punto;
import com.ufro.actividadtdd.dev.services.PuntoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/punto")
public class PuntoController {
    private PuntoService puntoService = new PuntoService();

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> getPoint(
            @RequestParam String x1, @RequestParam String y1,
            @RequestParam String x2, @RequestParam String y2
    ) {
        Map<String,Object> response = new HashMap<>();
        List<Punto> data = new ArrayList<>();

        Punto punto_one = new Punto(Integer.parseInt(x1), Double.parseDouble(y1));
        data.add(punto_one);

        Punto punto_two = new Punto(Integer.parseInt(x2), Double.parseDouble(y2));
        data.add(punto_two);

        String ecuacion = puntoService.calcularEcuacion(data);

        response.put("puntos", data);
        response.put("ecuacion", ecuacion);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }
}
