package com.ufro.actividadtdd.dev.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.text.DecimalFormat;
import java.util.List;

@AllArgsConstructor
@Getter
public class Ecuacion {
    private List<Punto> puntos;
    private double pendiente;
    private double promedio_x;
    private double promedio_y;
    private double suma_xy;
    private final int datos = 2;
    private double ordenada;

    public Ecuacion(List<Punto> puntos) {
        this.puntos = puntos;
    }

    public void calcular_valores_ordenada() {
        double x_cuadrado = 0;
        for (Punto punto : puntos) {
            suma_xy = suma_xy + (punto.getX() * punto.getY());
            this.promedio_x += punto.getX();
            this.promedio_y += punto.getY();
            x_cuadrado += (punto.getX() * punto.getX());
        }
        this.promedio_x = this.promedio_x / datos;
        this.promedio_y = this.promedio_y / datos;

        this.pendiente = (suma_xy - (datos * promedio_x * promedio_y)) / (x_cuadrado - (datos * (promedio_x * promedio_x)));
    }

    public void calcular_ordenada() {
        this.ordenada = this.promedio_y - (this.pendiente * datos);
    }

    public String regresion_lineal() {
        return "y = " + String.format("%.2f", this.ordenada)  + " + " + String.format("%.2f", this.pendiente)+ "x";
    }


}
