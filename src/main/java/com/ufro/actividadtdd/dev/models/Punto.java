package com.ufro.actividadtdd.dev.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Punto {
    private int x;
    private double y;
}