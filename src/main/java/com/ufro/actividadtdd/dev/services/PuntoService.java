package com.ufro.actividadtdd.dev.services;

import com.ufro.actividadtdd.dev.models.Ecuacion;
import com.ufro.actividadtdd.dev.models.Punto;

import java.util.List;

public class PuntoService {
    public String calcularEcuacion(List<Punto> puntos) {
        Ecuacion ecuacion = new Ecuacion(puntos);

        ecuacion.calcular_valores_ordenada();
        ecuacion.calcular_ordenada();

        return ecuacion.regresion_lineal();
    }
}
